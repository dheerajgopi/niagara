/**
 * Function submitCredentials
 * extracts product name, username, and password from the html form.
 * checks if the product name is valid, and returns if its invalid.
 * if product name is valid, it redirects to the relevant niagara website,
 * with the credentials.
 */
function submitCredentials() {
    // niagara links mapped to product names
    var productNames = {
                            'ipc': {
                                'link': 'niagara1.ipublishcentral.com',
                                'users': {
                                    'niagara': '!mp3l5y5@z015'
                                }
                            },
                            'services': {
                                'link': 'niagara2.ipublishcentral.com',
                                'users': {
                                    'niagara2': 'niagara2z'
                                }
                            }
                        };

    // extracts product name & credentials
    var product  = document.getElementById('Product').value;
    var userName = document.getElementById('user-name').value;
    var password = document.getElementById('user-password').value;
    var niagaraLink = productNames[product].link;

    // checks if username or password is empty
    if (!userName || !password) {
        document.getElementById('error-msg').innerHTML = 'Username or password field is empty!';
        return;
    }

    // validates username and password and shows error message if there is any error
    if (!validateLogin(product, userName, password, productNames)) {
        document.getElementById('error-msg').innerHTML = 'Invalid username or password!';
        return;
    }

    // removes error messages
    document.getElementById('error-msg').innerHTML = '';

    // form the link and redirect to niagara
    var link = 'http://' + userName + ':' + password + '@' + niagaraLink;
    window.location.replace(link);
}

/**
 * Function validateLogin
 * Validates the form and returns true if the credentials are valid.
 * Else the function will return false.
 */
function validateLogin(product, userName, password, productNames) {
    if (productNames[product].users[userName] === password) {
        return true;
    }
    return false;
}
